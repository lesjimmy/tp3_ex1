#ifndef DIRECTEUR_HPP
#define DIRECTEUR_HPP

#include "Employe.hpp"


class Directeur : public Employe
{
    public:
        Directeur();
        Directeur(std::string nom, std::string prenom, std::string adresse, int salaire, int prime):
            Employe(nom,prenom,adresse,salaire),
            prime(prime){}
        int donneRevenu(){
            return this->donneSalaire() + this->prime;
        }
        Employe* donneEncadrant(){
            return nullptr;
        }
        virtual ~Directeur();
    protected:
    private:
        int prime;
};

#endif // DIRECTEUR_HPP
