#ifndef LIVREUR_HPP
#define LIVREUR_HPP

#include "EmployeEncadre.hpp"


class Livreur : public EmployeEncadre
{
    public:
        Livreur();
        Livreur(std::string nom, std::string prenom, std::string adresse, int salaire, Employe* encadrant)
        :EmployeEncadre(nom, prenom, adresse, salaire, encadrant){}
        int donneRevenu(){
            return this->donneSalaire();
        }
        virtual ~Livreur();
    protected:
    private:
};

#endif // LIVREUR_HPP
