#ifndef ENTREPRISE_H
#define ENTREPRISE_H
#include <string>
#include <vector>

#include "Directeur.hpp"

class Entreprise
{
    public:
        Entreprise();
        Entreprise(std::string nom, std::string raisonSociale, std::string adresse, Directeur* dirigeant);
        void recruter(Employe* employe);
        Employe** donneEmployes(int* nbEmployes);
        virtual ~Entreprise();
    protected:
    private:
        std::string nom;
        std::string raisonSociale;
        std::string adresse;
        std::vector<Employe*> employes;
        Directeur* dirigeant;
};

#endif // ENTREPRISE_H
