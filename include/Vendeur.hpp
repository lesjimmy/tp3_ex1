#ifndef VENDEUR_H
#define VENDEUR_H

#include "EmployeEncadre.hpp"


class Vendeur : public EmployeEncadre
{
    public:
        Vendeur();
        Vendeur(std::string nom, std::string prenom, std::string adresse, int salaire, Employe* encadrant, int commission)
        :EmployeEncadre(nom, prenom, adresse, salaire, encadrant), commission(commission)
        {
        }
        int donneRevenu(){
            return this->donneSalaire() + this->commission;
        }
        virtual ~Vendeur();
    protected:
    private:
        int commission;
};

#endif // VENDEUR_H
