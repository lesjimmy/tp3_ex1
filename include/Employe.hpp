#ifndef EMPLOYE_H
#define EMPLOYE_H
#include <vector>
#include <string>

class Entreprise;
class EmployeEncadre;

class Employe
{
    public:
        Employe();
        Employe(std::string nom, std::string prenom, std::string adresse, int salaire);
        std::string donneNom(){return this->nom + " " +this->prenom;}
        int donneSalaire(){return this->salaire;}
        void ajouterEmployeEncadre(EmployeEncadre* emp);
        EmployeEncadre** donneSubordonnes(int* nbSubord);
        virtual ~Employe();
        virtual int donneRevenu() = 0;
        virtual Employe* donneEncadrant() = 0;
    protected:
    private:
        std::string nom;
        std::string prenom;
        std::string adresse;
        int salaire;
        Entreprise* entreprise;
        std::vector<EmployeEncadre*> encadre;
};

#endif // EMPLOYE_H
