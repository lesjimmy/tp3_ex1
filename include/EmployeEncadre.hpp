#ifndef EMPLOYEENCADRE_HPP
#define EMPLOYEENCADRE_HPP

#include "Employe.hpp"


class EmployeEncadre : public Employe
{
    public:
        EmployeEncadre();
        EmployeEncadre(std::string nom, std::string prenom, std::string adresse, int salaire, Employe* encadrant):Employe(nom, prenom, adresse, salaire), aPourEncadrant(encadrant)
        {
            aPourEncadrant->ajouterEmployeEncadre(this);
        }
        Employe* donneEncadrant()
        {
            return this->aPourEncadrant;
        }
        virtual ~EmployeEncadre();
    protected:
    private:
        Employe* aPourEncadrant;
};

#endif // EMPLOYEENCADRE_HPP
