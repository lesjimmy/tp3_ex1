#include "../include/Employe.hpp"

Employe::Employe()
{
    //ctor
}

Employe::Employe(std::string nom, std::string prenom, std::string adresse, int salaire){
    this->nom = nom;
    this->prenom = prenom;
    this->adresse = adresse;
    this->salaire = salaire;
}

EmployeEncadre** Employe::donneSubordonnes(int* nbSubord){
    *nbSubord = this->encadre.size();
    EmployeEncadre** employesArr = new EmployeEncadre*[*nbSubord];
    std::copy(this->encadre.begin(), this->encadre.end(), employesArr);
    return employesArr;
}

void Employe::ajouterEmployeEncadre(EmployeEncadre* emp){
    this->encadre.push_back(emp);
}

Employe::~Employe()
{
    //dtor
}
