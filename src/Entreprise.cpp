#include "..\include\Entreprise.hpp"

Entreprise::Entreprise()
{
    //ctor
}

Entreprise::Entreprise(std::string nom, std::string raisonSociale, std::string adresse, Directeur* dirigeant){
    this->nom = nom;
    this->raisonSociale = raisonSociale;
    this->adresse = adresse;
    this->dirigeant = dirigeant;
}

void Entreprise::recruter(Employe* employe){
    this->employes.push_back(employe);
}

Employe** Entreprise::donneEmployes(int* nbEmployes){
    *nbEmployes = this->employes.size();
    Employe** employesArr = new Employe*[*nbEmployes];
    std::copy(this->employes.begin(), this->employes.end(), employesArr);
    return employesArr;

}

Entreprise::~Entreprise()
{
    //dtor
}
